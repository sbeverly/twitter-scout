class AddContactedField < ActiveRecord::Migration
  def change
  	add_column :cached_users, :contacted, :boolean
  end
end
