class AddTwitterUserCache < ActiveRecord::Migration
  def change
  	create_table :cached_users do |t|
  		t.json	:user_info, required: true
  	end
  end
end