Rails.application.routes.draw do
  root 'twitter_api#index'
  get '/twitter_api/find_employees' => 'twitter_api#find_employees'
  get '/twitter_api/employee_details/:twitter_id' => 'twitter_api#employee_details'

  post '/twitter_api/contacted_employee/:twitter_id' => 'twitter_api#contacted'
  post '/twitter_api/direct_message/:screen_name' => 'twitter_api#direct_message'
  post '/twitter_api/tweet/' => 'twitter_api#tweet'
end