# README #

Assuming you have a Postgres connection running:

- git clone https://sbeverly@bitbucket.org/sbeverly/twitter-scout.git;
- cd /twitter-scout/twitter_search;
- rake db:create;
- rake db:migrate;
- rails s;

visit on localhost:3000 in your browser.

Note:  Direct Msg and Tweeting require individualized OAuth so they do not actually function but the controllers and methods are there.