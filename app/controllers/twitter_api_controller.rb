class TwitterApiController < ApplicationController
	include TwitterHelper

	def index
		
	end

	def find_employees
		@query = params[:company_name]

		if params[:page_num]
			@page = params[:page_num].to_i
		else
			@page = 1
		end

		@client = init_twitter_client

		@twitter_users = @client.user_search(@query, page: @page)

		@twitter_users.each do |user|
			CachedUser.create(twitter_id: user.id, user_info: user.as_json)
		end
	end

	def employee_details
		@cached_user = CachedUser.find_by(twitter_id: params[:twitter_id])

		@user_info = @cached_user.user_info

		@contacted = @cached_user.contacted

		@description = @user_info['description']
		@profile_pic = @user_info['profile_image_url']
		@name = @user_info['name']
		@location = @user_info['location']
		@urls = []
		@screen_name = @user_info['screen_name']
		@twitter_profile = "https://twitter.com/" + @user_info['screen_name']
		@total_tweets = @user_info['statuses_count']
		@followers = @user_info['followers_count']
		@following = @user_info['following']

		if @user_info['entities']['url']
			@user_info['entities']['url']['urls'].each { |url| @urls << url['expanded_url'] }
		end

		respond_to do |format|
			format.js
		end
	end

	def contacted
		if params[:contacted?] === 'yes'
			CachedUser.find_by(twitter_id: params[:twitter_id]).update(contacted: true)
		end

		redirect_to :controller => 'twitter_api', :action => 'employee_details', :twitter_id => params[:twitter_id]
	end

	def direct_message
		@client = init_twitter_client

		# @client.create_direct_message(params[:screen_name], params[:text])

		render js: "alert('Need Auth');"
	end

	def tweet
		@client = init_twitter_client

		# @client.update(params[:text])

		render js: "alert('Need Auth')"
	end
end